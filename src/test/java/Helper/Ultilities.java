package Helper;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Ultilities {
	public static String getProjectPath() {
		return System.getProperty("user.dir");
	}

	public static int generateRandomNumber(int min, int max) {
		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}

	public static String generateRandomString(int stringLength) {
		String characterString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String stringRandom = "";
		for (int i = 0; i < stringLength; i++) {
			stringRandom += characterString.charAt(generateRandomNumber(0, characterString.length() - 1));
		}
		return stringRandom;
	}
	
	public static String generateRandomString() {
		String characterString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String stringRandom = "";
		for (int i = 0; i < 10; i++) {
			stringRandom += characterString.charAt(generateRandomNumber(0, characterString.length() - 1));
		}
		return stringRandom;
	}
	
	public static String generateRandomGmail() {
		String characterString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String stringRandom = "";
		for (int i = 0; i < 10; i++) {
			stringRandom += characterString.charAt(generateRandomNumber(0, characterString.length() - 1));
		}
		return stringRandom + "@gmail.com";
	}

	public static String generateRandomStringNumber(int stringLength) {
		String numberCharacterString = "0123456789";
		String numberString = "";
		for (int i = 0; i < stringLength; i++) {
			numberString += numberCharacterString.charAt(generateRandomNumber(0, numberCharacterString.length() - 1));
		}
		return numberString;
	}
	
	public static String generateRandomStringNumber() {
		String numberCharacterString = "345678912";
		String numberString = "";
		for (int i = 0; i < numberCharacterString.length(); i++) {
			numberString += numberCharacterString.charAt(generateRandomNumber(0,numberCharacterString.length() - 1 ));
		}
		return numberString;
	}
	
	public static String removeSpecialCharacters(String str) {
		String specialCharacters = "/\\?%*:|\"<>.,;=";
		for (int i = 0; i < specialCharacters.length(); i++) {
			str = str.replace(Character.toString(specialCharacters.charAt(i)), "");
		}
		return str;
	}
	
	public static void sleep(int sleepTime) {
		try {
			Thread.sleep(sleepTime*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
    public static String replaceSpaceToHtmlStyle(String string) {
        return string.replaceAll(" ", "\u00a0");
    }
    
    public static void executeCommandLine(String command) {
    	Runtime rt = Runtime.getRuntime();
    	try {
			rt.exec(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static boolean doesListStringSorted(List<String> listString) {
    	List<String> tempList = listString;
    	Collections.sort(listString);
    	return tempList.equals(listString);
    }
    

}
