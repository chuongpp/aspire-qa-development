package Wrapper;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common.Constant;
import Helper.Logger;

public class WebElementWrapper {
	static Logger logger = new Logger(WebElementWrapper.class);

	protected By locator;
	protected String xpath;

	public WebElementWrapper(By locator) {
		this.locator = locator;
	}

	public WebElementWrapper(String xpath) {
		this.xpath = xpath;
		this.locator = By.xpath(xpath);
	}

	public WebElement waitForElementVisible(int waitingTime, boolean doesThrowException) {
		logger.info("Wait for element visible in: " + waitingTime + " second");
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), waitingTime);
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		long timeElapsed = stopwatch.getTime();
		while (timeElapsed <= waitingTime * 1000) {
			try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(this.locator));
				stopwatch.stop();
				return wait.until(ExpectedConditions.visibilityOfElementLocated(this.locator));
			} catch (StaleElementReferenceException error) {
				// TODO: handle exception
				timeElapsed = stopwatch.getTime();
			} catch (Exception error) {
				if (doesThrowException) {
					logger.warn("Throw Error: " + error);
					throw error;
				}
				break;
			}
		}
		stopwatch.stop();
		return wait.until(ExpectedConditions.visibilityOfElementLocated(this.locator));
	}

	public WebElement waitForElementVisible(int waitingTime) {
		return waitForElementVisible(waitingTime, false);
	}

	public WebElement waitForElementVisible() {
		return waitForElementVisible(Constant.WAIT_LONG_TIME);
	}

	public List<WebElement> waitForElementsVisible(int waitingTime, boolean doesThrowException) {
		logger.info("Wait for list elements visible in: " + waitingTime + " second");
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), waitingTime);
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		long timeElapsed = stopwatch.getTime();
		while (timeElapsed <= waitingTime * 1000) {
			try {
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(this.locator));
				stopwatch.stop();
				return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(this.locator));
			} catch (StaleElementReferenceException error) {
				// TODO: handle exception
				timeElapsed = stopwatch.getTime();
			} catch (Exception error) {
				if (doesThrowException) {
					logger.warn("Throw Error: " + error);
					throw error;
				}
				break;
			}
		}
		stopwatch.stop();
		return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(this.locator));
	}

	public List<WebElement> waitForElementsVisible(int waitingTime) {
		return waitForElementsVisible(waitingTime, false);
	}

	public List<WebElement> waitForElementsVisible() {
		return waitForElementsVisible(Constant.WAIT_LONG_TIME);
	}

	public WebElement waitForElementClickable(int waitingTime, boolean doesThrowException) {
		logger.info("Wait for element clickable in: " + waitingTime + " second");
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), waitingTime);
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		long timeElapsed = stopwatch.getTime();
		while (timeElapsed <= waitingTime * 1000) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(this.locator));
				stopwatch.stop();
				return wait.until(ExpectedConditions.elementToBeClickable(this.locator));
			} catch (StaleElementReferenceException error) {
				// TODO: handle exception
				timeElapsed = stopwatch.getTime();
			} catch (Exception error) {
				if (doesThrowException) {
					logger.warn("Throw Error: " + error);
					throw error;
				}
				break;
			}
		}
		stopwatch.stop();
		return wait.until(ExpectedConditions.elementToBeClickable(this.locator));
	}

	public WebElement waitForElementClickable(int waitingTime) {
		return waitForElementClickable(waitingTime, false);
	}

	public WebElement waitForElementClickable() {
		return waitForElementClickable(Constant.WAIT_LONG_TIME);
	}

	public WebElementWrapper getElementWithParameters(Object... parameters) {
		return new WebElementWrapper(By.xpath(MessageFormat.format(xpath.replaceAll("'", "''"), parameters)));
	}

	public void clickElement() {
		WebElement svgObject = waitForElementClickable();
		Actions builder = new Actions(DriverManager.getDriver());
		builder.click(svgObject).build().perform();
	}

	public void click() {
		logger.info("Click element with locator: " + locator);
		waitForElementClickable().click();
	}

	public void clickByJS() {
		logger.info("Click element by JS with locator: " + locator);
		JavascriptExecutor executor = (JavascriptExecutor) DriverManager.getDriver();
		executor.executeScript("arguments[0].click();", waitForElementVisible());
	}

	public void enterByJS(CharSequence... keysToSend) {
		logger.info("Click element by JS with locator: " + locator);
		JavascriptExecutor executor = (JavascriptExecutor) DriverManager.getDriver();
		executor.executeScript("arguments[0].setAttribute('value', '" + keysToSend +"')",  waitForElementVisible());
	}

	public void enter(CharSequence... keysToSend) {
		logger.info("Input value: " + String.valueOf(keysToSend) + " to element with locator: " + locator);
		this.clear();
		waitForElementVisible().sendKeys(keysToSend);
	}

	public void clear() {
		logger.info("Clear value of element with locator: " + locator);
		waitForElementVisible().clear();
	}

	public boolean isSelected() {
		logger.info("Is element with locator " + locator + " selected");
		return waitForElementVisible().isSelected();
	}

	public String getText() {
		logger.info("Get text of element with locator " + locator);
		return waitForElementVisible().getText();
	}

	public int getListSize() {
		logger.info("Get list size of element with locator " + locator);
		return waitForElementsVisible().size();
	}

	public List<String> getListTextOfListElement() {
		logger.info("Get list text of list elements with locator " + locator);
		List<WebElement> listElements = waitForElementsVisible();
		int sizelistElements = listElements.size();
		List<String> listText = new ArrayList<String>();
		for (int i = 0; i < sizelistElements; i++) {
			listText.add(listElements.get(i).getText());
		}
		return listText;
	}

	public void clickRandomElementInList(int randomIndex) {
		logger.info("Click random element in list");
		List<WebElement> listElements = waitForElementsVisible();
		listElements.get(randomIndex).click();
	}

	public boolean doesExist(int waitingTime, boolean doesThrowException) {
		logger.info("Is element with locator " + locator + " existed in " + waitingTime + " second");
		try {
			waitForElementVisible(waitingTime);
			return true;
		} catch (Exception error) {
			if (doesThrowException) {
				logger.warn("Throw Error: " + error);
				return true;
			}
			return false;
		}
	}

	public boolean doesExist(int waitingTime) {
		return doesExist(waitingTime, false);
	}

	public boolean doesExist() {
		return doesExist(Constant.WAIT_MID_TIME);
	}

	public boolean isDisplayed(int waitingTime, boolean doesThrowException) {
		logger.info("Is element with locator " + locator + " displayed in " + waitingTime + " second");
		if (doesExist(waitingTime, doesThrowException)) {
			return waitForElementVisible(waitingTime).isDisplayed();
		}
		return false;
	}

	public boolean isDisplayed(int waitingTime) {
		return isDisplayed(waitingTime, false);
	}

	public boolean isDisplayed() {
		return isDisplayed(Constant.WAIT_MID_TIME);
	}

	public boolean isClickable(int waitingTime, boolean doesThrowException) {
		logger.info("Is element with locator " + locator + " clickable in " + waitingTime + " second");
		try {
			waitForElementClickable(waitingTime);
			return true;
		} catch (Exception error) {
			if (doesThrowException) {
				logger.warn("Throw Error: " + error);
				return true;
			}
			return false;
		}
	}

	public boolean isClickable(int waitingTime) {
		return isClickable(waitingTime, false);
	}

	public boolean isClickable() {
		return isClickable(Constant.WAIT_MID_TIME);
	}

	public void moveMouseTo() {
		logger.info("Move mouse to element with locator " + locator);
		Actions actions = new Actions(DriverManager.getDriver());
		actions.moveToElement(waitForElementVisible());
		actions.perform();
	}

	public String getAttribute(String attribute) {
		logger.info("Get " + attribute + " attribute of element with locator " + locator);
		return waitForElementVisible().getAttribute(attribute);
	}
	
	public void setValue(String value) {
		logger.info("Set value " + value + " to element with locator " + locator);
		JavascriptExecutor executor = (JavascriptExecutor) DriverManager.getDriver();
		executor.executeScript("document.evaluate(\"" + xpath + "\", document,"
				+ " null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent=" + value);
	}
	
	public void scrollToViewElement() {
		((JavascriptExecutor) DriverManager.getDriver()).executeScript("arguments[0].scrollIntoView(true);", waitForElementVisible());
	}
}
