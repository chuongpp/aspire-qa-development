package PageObject.Pages;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class RegisterSelectMethodPage extends GeneralPage {
	WebElementWrapper btnGetStartWithMyInfo = new WebElementWrapper("//div[contains(@class,'q-pb-md')]//button//span[text()='Get Started']");
	WebElementWrapper btnGetStartWithStardardInfo = new WebElementWrapper("//div[contains(@class,'q-mt-lg')]//button//span[text()='Get Started']");
	
	public RegisterSelectMethodPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnGetStartWithMyInfo.waitForElementVisible();
		}
	}

	public RegisterSelectMethodPage() {
		btnGetStartWithMyInfo.waitForElementVisible(Constant.WAIT_LONG_TIME);
	}

	public void getStartWithMyInfo() {
		btnGetStartWithMyInfo.click();
	}
	
	public PersonViewPage gotoPersonViewPage() {
		btnGetStartWithStardardInfo.waitForElementClickable();
		btnGetStartWithStardardInfo.click();
		return new PersonViewPage();
	}
}
