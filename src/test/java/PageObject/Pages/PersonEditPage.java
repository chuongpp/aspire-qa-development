package PageObject.Pages;

import java.util.List;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class PersonEditPage extends GeneralPage{
	WebElementWrapper btnSubmit = new WebElementWrapper("//button[@type='submit']");
	WebElementWrapper txtNationality = new WebElementWrapper("//input[@placeholder='Select any of the following']");
	WebElementWrapper btnDropdownNationality = new WebElementWrapper("//input[@placeholder='Select any of the following']/../../following-sibling::div/i");
	WebElementWrapper optionDropbox = new WebElementWrapper("//div[@class='q-menu q-position-engine scroll q-menu--square']//div[@class='q-item__label' and text()='{0}']");
	WebElementWrapper btnDropdownGender = new WebElementWrapper("//input[@placeholder='Set your gender']/../../following-sibling::div/i");
	WebElementWrapper txtMonthYearOption = new WebElementWrapper("//span[text()='{0}']/../../parent::button");
	WebElementWrapper txtDayOption = new WebElementWrapper("//div[contains(@class,'q-date__calendar-days fit')]//button//span[text()='{0}']");
	WebElementWrapper iconLeftButton = new WebElementWrapper("//i[contains(@class,'fa-chevron-left')]");
	WebElementWrapper bodyCalender = new WebElementWrapper("//div[@class='q-date__header']");
	WebElementWrapper txtCalender = new WebElementWrapper("//input[@placeholder='Set your date of birth']");
	WebElementWrapper txtInterestedProduct = new WebElementWrapper("//label//div[@placeholder='Select your purpose of using Aspire']");
	
	public PersonEditPage(Boolean isDisplayed) {
		if (isDisplayed) {
			txtNationality.waitForElementVisible();
		}
	}

	public PersonEditPage() {
		txtNationality.waitForElementVisible(Constant.WAIT_LONG_TIME);
	}
	
	public VerifyEmailOtpPage gotoVerifyEmailOtpPage() {
		btnSubmit.click();
		return new VerifyEmailOtpPage();
	}
	
	public void openInputDayOfBirth() {
		txtCalender.click();
		bodyCalender.waitForElementVisible();
	}
	
    public void selectDayOfBirth(String dayOfBirth) {
    	txtDayOption.getElementWithParameters(dayOfBirth).click();
    }
    
    public void selectMonthOfBirth(String monthOfBirth, String currentMonth) {
    	txtMonthYearOption.getElementWithParameters(currentMonth).click();
        switch (monthOfBirth) {
        case "January":
        	txtMonthYearOption.getElementWithParameters("Jan").click();
            break;
        case "February":
        	txtMonthYearOption.getElementWithParameters("Feb").click();
            break;
        case "March":
        	txtMonthYearOption.getElementWithParameters("Mar").click();
            break;
        case "April":
        	txtMonthYearOption.getElementWithParameters("Apr").click();
            break;
        case "May":
        	txtMonthYearOption.getElementWithParameters("May").click();
            break;
        case "June":
        	txtMonthYearOption.getElementWithParameters("Jun").click();
            break;
        case "July":
        	txtMonthYearOption.getElementWithParameters("Jul").click();
            break;
        case "August":
        	txtMonthYearOption.getElementWithParameters("Aug").click();
            break;
        case "September":
        	txtMonthYearOption.getElementWithParameters("Sep").click();
            break;
        case "October":
        	txtMonthYearOption.getElementWithParameters("Oct").click();
            break;
        case "November":
        	txtMonthYearOption.getElementWithParameters("Nov").click();
            break;
        case "December":
        	txtMonthYearOption.getElementWithParameters("Dec").click();
            break;
        default:
            System.out.println("Can't find " + monthOfBirth + " element");
        }
    }

    public void selectYearOfBirth(String yearOfBirth, String currentYear) {
    	txtMonthYearOption.getElementWithParameters(currentYear).click();
    	iconLeftButton.waitForElementClickable();
        while (true) {
        	if (txtMonthYearOption.getElementWithParameters(yearOfBirth).doesExist()) {
        		txtMonthYearOption.getElementWithParameters(yearOfBirth).click();
        		break;
			}
        	else {
				iconLeftButton.click();
			}		
		}
    }
	
	public void selectNationality(String country) {
		txtNationality.enter(country);
		while (true) {
        	if (optionDropbox.getElementWithParameters(country).doesExist()) {
        		optionDropbox.getElementWithParameters(country).click();
        		break;
			}
        	else {
				btnDropdownNationality.click();
			}		
		}
	}
	
	public void selectGender(String gender) {
		btnDropdownGender.click();
		optionDropbox.getElementWithParameters(gender).click();
	}
	
	public void selectInterestedProduct(List<String> listInterestedProduct) {
		txtInterestedProduct.click();
		for (int i = 0; i < listInterestedProduct.size(); i++) {
			optionDropbox.getElementWithParameters(listInterestedProduct.get(i)).click();
		}
	}
	
	
}
