package PageObject.Pages;

import DataObjects.RegisterInfo;
import Wrapper.WebElementWrapper;

public class RegisterPage extends GeneralPage {
	WebElementWrapper txtFullName = new WebElementWrapper("//input[@name='full_name']");
	WebElementWrapper txtEmail = new WebElementWrapper("//input[@name='email']");
	WebElementWrapper txtPhone = new WebElementWrapper("//input[@name='phone']");
	WebElementWrapper radioButtonRole = new WebElementWrapper("//div[@aria-label='{0}']//*[name()='svg']");
	WebElementWrapper selectOption = new WebElementWrapper("//div[@class = 'q-virtual-scroll__content']//div[@class = 'q-item__label' and text() = '{0}']");	
	WebElementWrapper btnSelect = new WebElementWrapper("//input[@type='search']");
	WebElementWrapper dropBoxFollowing = new WebElementWrapper("//input[@type='search']");
	WebElementWrapper txtPromoCode = new WebElementWrapper("//input[contains(@placeholder,'promo code')]");
	WebElementWrapper checkBoxTerm = new WebElementWrapper(
			"//div[@role='checkbox']/div/div[contains(@class,'checkbox')]");
	WebElementWrapper btnSubmit = new WebElementWrapper("//button[@type='submit']");
	WebElementWrapper btnLogin = new WebElementWrapper("//a[@role='link']");

	public RegisterPage(Boolean isDisplayed) {
		if (isDisplayed) {
			txtFullName.waitForElementVisible();
		}
	}

	public RegisterPage() {
		txtFullName.waitForElementVisible();
	}

	public VerifyPhoneOtpPage gotoVerifyPhoneOtpPage() {
		btnSubmit.waitForElementClickable();
		btnSubmit.click();
		if (btnLogin.isDisplayed()) {
			btnLogin.click();
		}
		return new VerifyPhoneOtpPage();
	}

	public void enterFullName(String fullname) {
		if (fullname != null) {
			txtFullName.enter(fullname);
		}
	}

	public void enterEmail(String email) {
		if (email != null) {
			txtEmail.enter(email);
		}
	}

	public void enterPhoneNumber(String phoneNumber) {
		if (phoneNumber != null) {
			txtPhone.enter(phoneNumber);
		}
	}

	public void clickRole(String role) {
		if (role != null) {
			radioButtonRole.getElementWithParameters(role).clickElement();
		}
	}

	public void enterPromo(String promo) {
		if (txtPromoCode != null) {
			txtPromoCode.enter(promo);
		}
	}

	public void clickCheckTerm(boolean agreeTerm) {
		if (agreeTerm) {
			checkBoxTerm.click();
		}
	}

	public void selectHearingOption(String option) {
		if (option != null) {
			btnSelect.click();
			selectOption.getElementWithParameters(option).click();
		}
	}

	public String verifyBtnSubmitEnable() {
		return btnSubmit.getAttribute("aria-disabled");
	}

	public void inputAllInfo(RegisterInfo registerInfo) {
		enterFullName(registerInfo.getFullname());
		enterEmail(registerInfo.getEmail());
		enterPhoneNumber(registerInfo.getPhoneNumber());
		clickRole(registerInfo.getRole());
		selectHearingOption(registerInfo.getSelectHearingOption());
		enterPromo(registerInfo.getPromo());
		clickCheckTerm(registerInfo.isAgreeTerm());
	}
}
