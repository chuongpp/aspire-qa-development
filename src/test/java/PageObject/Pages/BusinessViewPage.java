package PageObject.Pages;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class BusinessViewPage extends GeneralPage{
	WebElementWrapper btnGetStart = new WebElementWrapper("//button[@type='submit']//span[text()='Get Started']");
	
	public BusinessViewPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnGetStart.waitForElementVisible();
		}
	}

	public BusinessViewPage() {
		btnGetStart.waitForElementVisible(Constant.WAIT_LONG_TIME);
	}
	
	public BusinessEditPage gotoBusinessEditPage() {
		btnGetStart.click();
		return new BusinessEditPage();
	}
}
