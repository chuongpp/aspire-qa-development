package PageObject.Pages;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class PersonViewPage extends GeneralPage{
	WebElementWrapper btnGetStart = new WebElementWrapper("//button[@type='submit']//span[text()='Get Started']");
	
	public PersonViewPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnGetStart.waitForElementVisible();
		}
	}

	public PersonViewPage() {
		btnGetStart.waitForElementVisible(Constant.WAIT_LONG_TIME);
	}
	
	public PersonEditPage gotoPersonEditPage() {
		btnGetStart.click();
		return new PersonEditPage();
	}
}
