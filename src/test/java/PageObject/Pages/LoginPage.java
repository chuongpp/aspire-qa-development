package PageObject.Pages;

import Wrapper.WebElementWrapper;

public class LoginPage extends GeneralPage{
	WebElementWrapper hyperLinkRegister = new WebElementWrapper("//a[contains(@href,'register')]");
	
	public LoginPage(Boolean isDisplayed) {
		if (isDisplayed) {
			hyperLinkRegister.waitForElementVisible();
		}
	}

	public LoginPage() {
		hyperLinkRegister.waitForElementVisible();
	}

	public RegisterPage goToRegisterPage() {
		hyperLinkRegister.click();
		return new RegisterPage();
	}
}
