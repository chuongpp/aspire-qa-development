package Test.HomePage;

import java.awt.AWTException;
import java.time.Month;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.etsi.uri.x01903.v13.CompleteCertificateRefsType;
import org.testng.annotations.Test;

import DataObjects.RegisterInfo;
import DataObjects.RoleOptions;
import Helper.Logger;
import Helper.Ultilities;
import PageObject.Pages.BusinessEditPage;
import PageObject.Pages.BusinessViewPage;
import PageObject.Pages.CompleteRegisterPage;
import PageObject.Pages.IdentifyOnfidoPage;
import PageObject.Pages.IdentifyViewPage;
import PageObject.Pages.IncorporateSelectorPage;
import PageObject.Pages.LoginPage;
import PageObject.Pages.PersonEditPage;
import PageObject.Pages.PersonViewPage;
import PageObject.Pages.RegisterPage;
import PageObject.Pages.RegisterSelectMethodPage;
import PageObject.Pages.VerifyEmailOtpPage;
import PageObject.Pages.VerifyPhoneOtpPage;

public class HomePage extends BaseTest {
	Logger logger = new Logger(HomePage.class);

	@Test(testName = "DA_LOGIN_TC001", description = "Verify that user can login specific repository successfully via Dashboard login page with correct credentials")
	public void TestCase_001() throws AWTException {
		//prepare data
		String fullname = Ultilities.generateRandomString();
		String email = Ultilities.generateRandomGmail();
		String phoneNumber = Ultilities.generateRandomStringNumber();
		String role = RoleOptions.APPOINTED_DIRECTOR.getValue();
		String selectHearingOption = "Others";
		String promo = Ultilities.generateRandomStringNumber();
		boolean agreeTerm = true;
		String currentMonth = "January";
		String monthOfBirth = "August";
		String currentYear = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));;
		String yearOfBirth = "2000";
		String dayOfBirth = "19";
		String gender = "Male";
		String country = "Viet Nam";
		String industry = "Retail Services";
		String subIndustry = "Automotive & Cars";
		String registrationType = "Sole proprietorship";
		List<String> expectedInterestedProducList = new ArrayList<String>(
				Arrays.asList("Credit Line", "Debit Account", "Others"));
		RegisterInfo registerInfo = new RegisterInfo(fullname, email, phoneNumber, role, selectHearingOption, promo, agreeTerm);
		//navigate to default url
		LoginPage loginPage = new LoginPage();
		
		//navigate to register page
		RegisterPage registerPage = loginPage.goToRegisterPage();
		
		//Input all required data
		registerPage.inputAllInfo(registerInfo);
		
		//Navigate to Verify Phone OTP page
		VerifyPhoneOtpPage verifyPhoneOTPPage = registerPage.gotoVerifyPhoneOtpPage();
		
		//Input default OTP : 1234
		CompleteRegisterPage completeRegisterPage = verifyPhoneOTPPage.inputDefaultOTP();
		
		//Navigate to Incorporate Selector Page
		IncorporateSelectorPage incorporateSelectorPage = completeRegisterPage.gotoIncorporateSelectorPage();
		
		//Navigate to Register Select Method Page
		RegisterSelectMethodPage registerSelectMethodPage = incorporateSelectorPage.gotoRegisterSelectMethodPage();
		
		//Navigate to Person View Page
		PersonViewPage personViewPage = registerSelectMethodPage.gotoPersonViewPage();
		
		//Navigate to Person Edit Page
		PersonEditPage personEditPage = personViewPage.gotoPersonEditPage();
		
		//Select day of birth
		personEditPage.openInputDayOfBirth();
		personEditPage.selectYearOfBirth(yearOfBirth, currentYear);
		personEditPage.selectMonthOfBirth(monthOfBirth, currentMonth);
		personEditPage.selectDayOfBirth(dayOfBirth);
		
		//Select gender
		personEditPage.selectGender(gender);
		
		//Select Interested Product
		personEditPage.selectInterestedProduct(expectedInterestedProducList);
		
		//Select nationality
		personEditPage.selectNationality(country);
		
		//Navigate to Verify Email Otp Page
		VerifyEmailOtpPage verifyEmailOtpPage = personEditPage.gotoVerifyEmailOtpPage();
		
		//Input default OTP : 1234
		BusinessViewPage businessViewPage = verifyEmailOtpPage.inputDefaultOTP();
		
		//Navigate to Business Edit Page
		BusinessEditPage businessEditPage = businessViewPage.gotoBusinessEditPage();
		
		//Input Bussiness Name
		businessEditPage.enterBussinessName(fullname);
		
		//Select Registration Type
		businessEditPage.selectRegistrationType(registrationType);
		
		//Select Registration Business Industry
		businessEditPage.selectRegisterBusinessIndustry(industry);
		
		//Select Registration Business SubIndustry
		businessEditPage.selectRegisterBusinessSubIndustry(subIndustry);
		
		//Input random UEN number
		businessEditPage.inputUENNumber();
		
		//Navigate to Identify View Page
		IdentifyViewPage identifyViewPage = businessEditPage.gotoIdentifyViewPage();
		
		//Navigate to Identify Onfido Page
		IdentifyOnfidoPage inIdentifyOnfidoPage = identifyViewPage.gotoIdentifyOnfidoPage();
		
		//Navigate to check verify
		inIdentifyOnfidoPage.gotoVerify();
		logger.reportStep("VP. Verify that Dashboard Mainpage appears");
	}
}
