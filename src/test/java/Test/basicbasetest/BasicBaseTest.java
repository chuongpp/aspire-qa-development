package Test.basicbasetest;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Common.Constant;
import Helper.DateHelper;
import Helper.Listener;
import Helper.Logger;
import Helper.Reporter;
import Helper.Ultilities;
import Wrapper.DriverManager;

@Listeners(Listener.class)
public class BasicBaseTest {
	static String browserString;
	static String modeString;
	public SoftAssert softAssert;
	String folerPathString;
	Logger logger = new Logger(BasicBaseTest.class);
	
	@Parameters({ "browser" , "mode"})
	@BeforeSuite
	public void setUpSuite(String browser, String mode, ITestContext context) {
		browserString = browser;
		modeString = mode;
		String timeTest = new DateHelper().formatDateTime(Constant.FORMAT_DATE_TIME_TO_REPORT);
		logger.info("--------Start Suite " + context.getName() + "--------");
		folerPathString = Ultilities.getProjectPath() + "\\results\\" + context.getName() + "\\" + browser + "_" + mode + "\\" + timeTest;
		Reporter.createReporter(folerPathString);
		File file = new File(Constant.LOG4J_LOG_FILE);
		if(file.exists()) {
			file.deleteOnExit();
		}
	}
	
	@BeforeMethod
	public void setUpBasic(Method method){
		softAssert = new SoftAssert();
		String testName = method.getAnnotation(Test.class).testName();
		String testDescription = method.getAnnotation(Test.class).description();
		logger.info("Start Test Case: " + testName);
		Reporter.createTest(testName + " - " + testDescription);
		logger.info("Pre-condition");
		DriverManager.createDriver(browserString, modeString);
		logger.reportStep("Step1. Navigate to Dashboard login page");
		DriverManager.navigateTo(Constant.URL);
	}
	
	@AfterSuite
	public void tearDownSuite() {
		try {
			FileUtils.copyFile(new File(Constant.LOG4J_LOG_FILE), new File(folerPathString + "/log.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void tearDownBasic(Method method) {
		String testName = method.getAnnotation(Test.class).testName();
		DriverManager.quit();
		logger.info("End Test Case: " + testName);
		logger.info("-----------------------------------------------------------");
	}
    
    public void acceptAlert() {
    	DriverManager.acceptAlert();
    }
}
