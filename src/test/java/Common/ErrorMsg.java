package Common;

public class ErrorMsg {
	// Alert
	public static final String INVALID_ACCOUNT = "Username or password is invalid";
	public static final String PLEASE_ENTER_USERNAME = "Please enter username";
	public static String CAN_NOTE_DELETE_PAGE_ERROR_MSG = "Cannot delete page '%s' since it has child page(s).";
	public static String DUPLICATE_NAME_PAGE_ERROR_MSG = "%s already exist. Please enter a diffrerent name.";

	// Add edit panel dialog
	public static final String ERROR_DISPLAY_NAME_REQUIRED = "Display Name is a required field.";
	public static final String ERROR_CHARACTERS_DISPLAY_NAME = "Invalid display name. The name cannot contain high ASCII characters or any of the following characters: /:*?<>|\"#[]{}=%;";
	
}
