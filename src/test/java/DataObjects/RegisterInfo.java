package DataObjects;

public class RegisterInfo {
	private String fullname;
	private String email;
	private String phoneNumber;
	private String role;
	private String selectHearingOption;
	private String promo;
	private boolean agreeTerm;
	
	public RegisterInfo(String fullname, String email, String phoneNumber, String role, String selectHearingOption,
			String promo, boolean agreeTerm) {
		this.fullname = fullname;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.role = role;
		this.selectHearingOption = selectHearingOption;
		this.promo = promo;
		this.agreeTerm = agreeTerm;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSelectHearingOption() {
		return selectHearingOption;
	}

	public void setSelectOption(String selectHearingOption) {
		this.selectHearingOption = selectHearingOption;
	}

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}	
	
	
	public boolean isAgreeTerm() {
		return agreeTerm;
	}

	public void setAgreeTerm(boolean agreeTerm) {
		this.agreeTerm = agreeTerm;
	}
}
